Andy
====

This *is* a test

The :py:func:`enumerate` function is described below.

* This is a bulleted list.
* It has two items, the second
  item uses two lines.

1. This is a numbered list.
2. It has two items too.

#. This is a numbered list.
#. It has two items too.


* this is
* a list

  * with a nested list
  * and some subitems

* and here the parent list continues

more stuff
----------

| These lines are
| broken exactly like in
| the source file.

This is a normal text paragraph. The next paragraph is a code sample::

    from django.shortcuts import render

    # Create your views here.

    def home(request):
        return render(request,
                      'home.html')

    def demo1(request):
        return render(request,
                      'demo1.html')
    def demo2(request):
        return render(request,
                      'demo2.html')


This is a normal text paragraph again.

=====  =====  =======
A      B      A and B
=====  =====  =======
False  False  False
True   False  False
False  True   False
True   True   True
=====  =====  =======

This is a paragraph that contains `a link`_.

.. _a link: http://google.com/

literal include
---------------

.. literalinclude:: ./conf.py
    :linenos:
    :language: python
    :lines: 19-24, 66-67
    :lineno-start: 19

a note
------

.. note::
    There are many other Info fields but they may be redundant:
        * param, parameter, arg, argument, key, keyword: Description of a
          parameter.
        * type: Type of a parameter.
        * raises, raise, except, exception: That (and when) a specific
          exception is raised.
        * var, ivar, cvar: Description of a variable.
        * returns, return: Description of the return value.
        * rtype: Return type.
