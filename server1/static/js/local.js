/**
 * Created by Andy on 11/03/2016.
 */

$(document).ready(function () {
    // executes when HTML-Document is loaded and DOM is ready.

    $('div.json').each(function( index, value ) {
        $(this).html(syntaxHighlight( $(value).text() ));
    });

})
