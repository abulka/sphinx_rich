// Simple JSON syntax highlighting

function syntaxHighlight(json) {
    json = json.replace(/&/g, '&').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        var br = '';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
                br = '<br>'
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return br + '<span class="' + cls + '">' + match + '</span>';
    });
}

/*
  Example usage:

function output(inp) {
    document.body.appendChild(document.createElement('pre')).innerHTML = inp;
}

var obj = {a:1, 'b':'foo', c:[false,'false',null, 'null', {d:{e:1.3e5,f:'1.3e5'}}]};
var str = JSON.stringify(obj, undefined, 4);

output(str);
output(syntaxHighlight(str));
*/

