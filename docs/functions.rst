Functions
=========

explicit
--------

.. py:function:: enumerate(sequence[, start=0])

   Return an iterator that yields tuples of an index and an item of the
   *sequence*. (And so on.)

.. py:class:: Base(object)

   Is a cool class.

auto individual functions
-------------------------

.. autofunction:: server1.views.demo1

.. autofunction:: lib.validate_lists.ensure_dicts_have_keys
    :noindex:

auto function in a nested package
---------------------------------

.. autofunction:: lib.popular_repos.repos_static.get_repos_list

