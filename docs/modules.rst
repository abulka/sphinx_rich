Modules
=======

explicit
--------

Not really supported.  There is a directive but it changes the current 'namespace' or something.

auto module
-----------

More auto doco here http://www.sphinx-doc.org/en/stable/ext/autodoc.html

Use ``:noindex:`` when you have already autodoc'd something and its being added repeatedly
to the documentation indexes etc.  Don't want duplicates.

.. automodule:: server1.views
  :members:
  :undoc-members:
  :noindex:

auto module again
-----------------

.. automodule:: lib.validate_lists
  :members:
  :undoc-members:

.. automodule:: lib.tree
  :members:
  :undoc-members:

.. automodule:: lib.util
  :members:
  :undoc-members:

auto module tricky
------------------

This will just pull out the ``lib.popular_repos.repos_static`` module's docstring
and no members are listed cos we don't have the ``:members:`` directive

Watch out, the directive ``:special-members:`` lists things with __

lib.popular_repos.repos_static
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: lib.popular_repos.repos_static

module variables
~~~~~~~~~~~~~~~~

.. automodule:: lib.sample_module
  :members:
  :undoc-members:

