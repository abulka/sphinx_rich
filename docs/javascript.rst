Javascript
==========

To autodoc javascript we need to do some preprocessing

https://github.com/erikrose/sphinx-js

install
-------

To install::

    pip install sphinx_js
    npm install -g jsdoc


P.S. sudo not needed.

then add ``sphinx_js`` to extensions::

    extensions = [
        'sphinx_js',
        ]

and add a js_source_path::

    js_source_path = '../server1/static/js/'

and make sure there is at least one javascript file?

Make sure all comments have TWO ASTERISKS ``/**`` on the first line::

    /**
     * Just a function1
     */
    function function1() {

    }

not::

    /*
     * Just a function1
     */
    function function1() {

    }

auto examples
-------------

.. js:autofunction:: linkDensity

.. js:autofunction:: function1

.. js:autofunction:: function2

.. js:autofunction:: repomgr

explicit
--------

.. js:function:: $.getJSON(href, callback[, errback])

   :param string href: An URI to the location of the resource.
   :param callback: Get's called with the object.
   :param errback:
       Get's called in case the request fails. And a lot of other
       text so we need multiple lines
   :throws SomeError: For whatever reason in that case.
   :returns: Something
