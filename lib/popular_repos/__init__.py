# import repos_static
#
# def get_repos_list(collection):
#     """
#     Return a list of repos, either static or dynamic.  Dynamic repo is '2'
#
#     :param repo_collection: string indicating which repo collection is wanted
#     :return: either a list of dicts or raises exception
#     """
#     if collection in ['2']:
#         data = repos_dynamic.get_repos_list(collection)
#     else:
#         data = repos_static.get_repos_list(collection)
#     return data