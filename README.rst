Sphinx Rich
-----------

This project is a **test** python 3 django project with sphinx support.

Its aims are to:

- not do any sibling directory referencing   **done**
- is a django project itself  **done**
- uses the _docs_ subdir for sphinx doco    **done**
- Uses plantweb in order to render the UML diagrams   **done**
- Uses nice google style docstrings (needs a plugin to convert and process properly in sphinx)   **done**
- Parses javascript   **done**

- uses webhooks from bitbucket to readthedocs
 
Readthedocs version of this doco http://sphinx-rich.readthedocs.io/en/latest/

Setting up dev environment::

    548  git clone https://bitbucket.org/abulka/sphinx_rich.git
    549  cd sphinx_rich
    550  conda create -n py36 python=3
    551  . activate py36
    552  pip install -r requirements.txt
    553  python manage.py migrate
    554  cd docs
    556  npm install -g jsdoc
    557  make html

Deployment to readthedocs.org
-----------------------------

Two problems:

    - readthedocs doesn't like python 3 pep484 type hints and thus google doc string
      not understood and skipped
    - readthedocs doesn't support installing and running ``jsdoc`` thus the javascript
      auto docstrings aren't read, and the build process fails.

