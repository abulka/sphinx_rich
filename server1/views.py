from django.shortcuts import render

# Create your views here.

def home(request):
    return render(request,
                  'home.html')

def demo1(request):
    return render(request,
                  'demo1.html')
def demo2(request):
    return render(request,
                  'demo2.html')
