
/**
 * Just a function2
 */
function function2(a,b,c,d) {

}


/**
 * Not too shabby, right?
 */
function repomgr(user, repo_name)
{
    // Repo functionality

    var github;             // github object (Github library)
    var repo;               // github repo object (Github library)

    /**
     * Not too shabby2, right?
     */
    function run(user_func) {
        // grabs github tree, converts it into custom all_files list, and calls user_func(err, all_files)
        github = new Github();
        repo = github.getRepo(user, repo_name);
        _call_github(user_func);
    }

    function _call_github(user_func) {
        // call github, then pass 'all_files' object list to user_func
        var all_files = [];
        repo.getTree('master?recursive=true', function (err, tree) {
            if (err == undefined) {
                //console.log(tree);
                all_files = _tree_to_all_files(tree);
            }
            user_func(err, all_files);
        });
    }

    function _tree_to_all_files(tree) {
        // converts 'tree' to 'allfiles' custom list of path,url dictionaries
        var all_files = [];
        $.each(tree, function (index, value) {
            if (value.path.endsWith('.py')) {
                all_files.push({
                    'path': value.path,
                    'url': value.url
                });
            }
        });
        return all_files;
    }

    return {
        run: run,
    }

}
