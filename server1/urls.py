from django.conf.urls import url
from django.contrib import admin
from server1 import views
admin.autodiscover()

urlpatterns = [
    url(r'^$', views.home, name='server1_home'),

    url(r'^demo1$', views.demo1, name='demo1'),
    url(r'^demo2$', views.demo2, name='demo2'),
]
