import inspect

# Python execution stack analysis util - tells you who called current function etc.
def whosdaddy(): return inspect.stack()[2][3]
def whosgranddaddy(): return inspect.stack()[3][3]

