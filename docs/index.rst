.. sphinx_rich documentation master file, created by
   sphinx-quickstart on Wed May 31 17:34:39 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to sphinx_rich's documentation!
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   README <readme_link>
   andy
   images
   functions
   modules
   docstrings
   uml_plantweb
   markdown.md
   javascript

official Sphinx doco
--------------------

http://www.sphinx-doc.org/en/stable/contents.html


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
