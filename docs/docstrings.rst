Doc Strings
===========

If you enable the **napoleon** extension in **conf.py** then you can use the nicer
google style docstring format.

napoleon
--------

I think it used to be called ``sphinxcontrib.napoleon`` but now is bundled with
sphinx and called ``sphinx.ext.napoleon`` viz::

    extensions = ['sphinx.ext.autodoc',
        'sphinx.ext.todo',
        'sphinx.ext.napoleon',
        'sphinx.ext.viewcode']

Napoleon (google and numy supported) docstring directives https://sphinxcontrib-napoleon.readthedocs.io/en/latest/
The Sphinx version of this page is http://www.sphinx-doc.org/en/stable/ext/napoleon.html

P.S. All the Sphinx bundled extensions are listed http://www.sphinx-doc.org/en/stable/extensions.html

google style
------------

Got this from http://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html

.. automodule:: lib.example_google
  :members:
  :undoc-members:

