Plantweb
========

Plantweb is a python library and command line, that invokes
the **remote plantUML server**.

This means you don't need to install
and run a local java ``plantuml.jar`` when generating
documentation using Sphinx

This allows plantuml to be rendered and overrides plantuml native directives like ``.. uml::``

* Handy if you can't be bothered setting that up
  and have online access.

* Handy if you are hosting on ReadTheDocs_ thus
  may not have the ability to run java bits and pieces there

See http://plantweb.readthedocs.io/index.html


External hyperlinks, like Python_.

.. _Python: http://www.python.org/
.. _ReadTheDocs: http://www.readthedocs.org/


example
-------

.. uml::

   Alice -> Bob: Authentication Request
   Bob --> Alice: Authentication Response

   Alice -> Bob: Another authentication Request
   Alice <-- Bob: another authentication Response

more uml
--------

.. uml::

    @startuml

    skin BlueModern
    title testing plantml here
    box "CDCI Orchestration"
    participant "CDCI" as A
    participant "Billing Adapter" as B
    participant "OBA" as C
    end box

    activate A
    A -> B: "GET /parallels/resources/v1/<uuid>"
    B -> C: GetOrder_API(OrderID)
    B <-- C: &#91;&#91; ..., "CP", ... &#93;&#93;

    A <-- B: "HTTP/1.1 code 200"
    deactivate A

    @enduml

well I hope the diagram above was generated.

gituml thoughts
---------------

1. Being able to run plantuml from a jar file locally
   is something that sphinx does, and is therefore something
   that GitUML should be able to do.

       * Heroku would need to support this in production too.

       * Would make GitUml independent of PlantUml

2. Being able to use the remote PlantUml server via a nicer
   interface (plantweb) may be useful to GitUml in its current form.


