# Validation

def ensure_dicts_have_keys(list_of_dicts, keys_spec, warn_extra_keys=False):
    """Ensure that dictionaries in list 'znodes' have the expected keys.  'keys_spec' is a code for specifying which keys"""
    keys = []
    for c in keys_spec:
        if c == 'P':
            keys.append('path')
        elif c == 'U':
            keys.append('url')
        elif c == 'N':
            keys.append('name')
        elif c == 'A':
            keys.append('ppath')
        elif c == 'K':
            keys.append('package')
        elif c == 'O':
            keys.append('open')
        else:
            raise RuntimeError('Unknown keys_spec character %s' % c)

    # Ensure all expected dict keys are present
    if len(list_of_dicts) > 0:
        sample_dict = list_of_dicts[0]
        for key in keys:
            if not key in sample_dict.keys():
                raise RuntimeError('Missing key {} in dict {} when it should be in {} format containing keys {}'.format(
                    key, sample_dict, keys_spec, keys,
                ))

        if warn_extra_keys:
            # Ensure there are no extra dict keys hanging around
            if len(sample_dict.keys()) != len(keys):
                raise RuntimeError('Some of these keys {} is unexpected in dict {} when it should be in {} format containing keys {}'.format(
                    sample_dict.keys(), sample_dict, keys_spec, keys,
                ))
    return True

def ensure_is_list_of_strings(paths_list):
    """Ensure 'paths_list' is a list of strings"""
    ensure_is_list(paths_list)

    if len(paths_list) > 0:
        sample_item = paths_list[0]
        if not isinstance(sample_item, basestring):
            raise RuntimeError("paths_list item {} should be of type string not {}".format(
                sample_item, type(sample_item),
            ))
    return True

def ensure_is_list(paths_list):
    if not isinstance(paths_list, list):
        raise RuntimeError("paths_list {} should be of type list not {}".format(
            paths_list, type(paths_list),
        ))
    return True

